Dear Uwe, dear CRAN Team,

thank you very much for your advice, I would never have identified the runtime 
of more than 10 minutes as the issue to address without your help.

I've reduced the number of tests that run on CRAN significantly, which is not
a problem for us, since we run daily tests on our own platforms and on diverse
heterogeneous developer machines (including Windows, Linux and a M3 Mac). Also,
I've replaced large parts of the vignette, which was very reduced, already. Now,
it merely links to the elaborated tutorials on the internet.

Using this, I could reduce the overall runtimes to 445 s (R version 4.4.0 
alpha 2024-03-26 r86209 ucrt), 229 s (R version 4.2.3 2023-03-15 ucrt), and
311 s (R version 4.3.3 2024-02-29 ucrt). Also, r-hub now comes to an ok instead
of a timeout (linux-x86_64-ubuntu-gcc).

Also, local tests have run again on one machine (R version 4.3.2 2023-10-31,
aarch64-apple-darwin20) still are all Ok (0 errors, 0 warnings, 0 notes), with
and without `as-cran`.

Finally, I hope, that now, everything is fine. Maybe, this was my personal
Passiontide for 2024. Happy Easter and Thank you again!!

Stephan


---- last email from Uwe Ligges

> Thanks, we see:
> 
> >    Overall checktime 22 min > 10 min
> 
> mainly from
> 
> * checking tests ... [12m] OK
>  Running 'testthat.R' [12m]
> 
> Please reduce the test timings by using
> - small toy data only
> - few iterations
> - or by running less important tests only conditionally if some environment variable is set that you only define on your machine?
> 
> 
> Please fix and resubmit.
> 
> Best,
> Uwe Ligges


---- former CRAN comments for this submission
> Dear CRAN team,
> 
> here a re-submission. Sorry, that this was necessary, but 
> the CRAN pre-tests suffer from the same connectivity
> problems to the homepage of the ministry of education and research of Germany,
> that I had described in my first submission of this version, and so I had now
> pre-tests failed.
> 
> I have removed the links to the BMBF in favor of just naming the `BMBF` without 
> proper links, but will open a discussion about this at the `R-package-devel` 
> mailing list, after the Spring Break, because I think, one should find out the
> reason, since the `BMBF` funds many research projects in Germany.
> 
> I have re-run the 3 `winbuilder` `check_win_*()` tests, and they do no not show 
> any errors, warnings or notes, any more. Since this issue with the `README.md`
> file is the only change I did, I've only re-run `winbuilder`, the `MacBuilder` 
> (both, "release" and "development"), and local tests on one machine, which 
> again succeeded:
> 
>   - `R version 4.3.2 (2023-10-31) aarch64-apple-darwin20 (64-bit)` 
>     (`R CMD check --as-cran` and `devtools::check()` with 
>     `NOT_CRAN=true`)
> 
> In the hope, that now, everything works, happy Easter days, thank you,
> 
> Stephan Struckmann
> 
> > 
> > Dear CRAN team,
> >
> > I'm submitting a new release of `dataquieR`, version `2.1.0`. It contains 
> > several usability improvements and new support for summaries of data quality 
> > reports.
> >
> > Also, a metadata about the scale level of items is now supported to direct the
> > report generation, so that the output is now more focused.
> > 
> > Thank you for running `CRAN`.
> > 
> > ## R CMD check results:
> > 
> > I have run `R CMD check`s on `r-hub` (x86_64-w64-mingw32, all other platforms
> > had a time-out, maybe caused by not skipping `skip_on_cran()` tests), the 
> > three `check_win_*` versions, and the `MacBuilder` (both, "release" and 
> > "development").
> > 
> > On our local build servers, we run two different containers with:
> > 
> > >   - `R version 4.3.2 (2023-10-31) x86_64-pc-linux-gnu (64-bit)`
> > >   - `R version 4.3.3 (2024-02-29) x86_64-pc-linux-gnu (64-bit)`
> > 
> > Additionally, we've run the checks on local developer machines:
> > 
> >  - `R version 4.3.2 (2023-10-31) aarch64-apple-darwin20 (64-bit)`
> >  - `R version 4.3.3 (2024-02-29) x86_64-pc-linux-gnu`
> >  - `R version 4.3.3 (2024-02-29 ucrt)  x86_64-w64-mingw32/x64 (64-bit)`
> > 
> > ### Except from R-Hub, all platforms gave
> >  
> > ```
> > 0 errors | 0 warnings | 0 notes
> > ```
> > 
> > or 
> > 
> > ```
> > [...]
> > Found the following (possibly) invalid URLs:
> >   URL: https://www.bmbf.de/
> >     From: README.md
> >     Status: 400
> >     Message: Bad Request
> > [...]
> > 0 errors | 0 warnings | 1 notes
> > ```
> > (obviously, `winbuilder` cannot reach one of our funding organizations,
> > but the link has been tested valid at 2024-03-27 4 PM CET)
> > 
> > The one running `r-hub` job gave
> > 
> > ```
> > [...]
> > * checking HTML version of manual ... [56s] NOTE
> > Skipping checking math rendering: package 'V8' unavailable
> > [...]
> > * checking for non-standard things in the check directory ... NOTE
> > Found the following files/directories:
> >   ''NULL''
> > [...]
> > * checking for detritus in the temp directory ... NOTE
> > Found the following files/directories:
> >   'lastMiKTeXException'
> > [...]
> > Status: 3 NOTEs
> > ```
> > 
> > I hope, that everything runs, thank you for processing my uploaded package.
> > 
> > Stephan Struckmann
> > 
